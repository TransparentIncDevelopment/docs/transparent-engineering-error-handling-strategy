#[cfg(feature = "substrate")]
pub mod substrate;

use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Error: Transaction {0} not signed.")]
    Unsigned(TransactionId),
    #[error("Error: Non-superuser (UserId {0}) attempted to register members.  Only superusers may register members.")]
    OnlySuperuserMayRegisterMembers(UserId),
    #[error("Error: Create amount must be greater than zero.  Attempted to create {0} Xand.")]
    MintAmountMustBeMoreThanZero(Balance),
    #[error("Error: Redeem amount must be greater than zero.  Attempted to redeem {0} Xand.")]
    SmeltAmountMustBeMoreThanZero(Balance),
    #[error("Error: A vote ({0}) was already submitted by this Participant ({1}).")]
    VoteAlreadySubmittedForAccount(Vote, Account),
    // Propagating third party errors should only be done strictly as necessary.
    // When propagating a third party error into the canonical error set, here is
    // an example of properly preparing it without additional dependencies on consumers.
    #[error(transparent)]
    WrappedThirdPartyError(WrappedThirdPartyError),
}

// Creating a new type to wrap third party errors will prevent consumers of the domain error
// from requiring knowledge of a third party error's properties in order to use the domain error.
// A wrapped third party error should be boxed as a dynamic Error + Send + Sync object, and if that
// is not possible, then stringifying it is an acceptable alternative.
#[derive(Debug, Error)]
pub enum WrappedThirdPartyError {
    // To prevent creating additional dependencies but maintain full fidelity of errors,
    // If possible, you can box the third party error as Box<dyn std::error::Error + Send + Sync>.
    // Send + Sync are required to prevent compiler issues for errors that are passed along threads.
    // This can happen implicitly during async code, see: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=91b200508717c2896ce1b3caec8a00f8
    #[error("{:?}", 0)]
    Boxed(Box<dyn std::error::Error + Send + Sync>),
    // If it is not possible to store the boxed error because it does not implement the Error, Send, and Sync traits,
    // then it is acceptable to stringify the error contents (via display or debug) as an alternative.
    #[error("{0}")]
    Stringified(String),
    // ...
}
impl WrappedThirdPartyError {
    pub fn new_boxed<T: 'static + std::error::Error + Send + Sync>(err: T) -> Self {
        Self::Boxed(Box::new(err))
    }

    pub fn new_debug<T: std::fmt::Debug>(err: T) -> Self {
        Self::Stringified(format!("{:?}", err))
    }

    pub fn new_stringified<T: std::fmt::Display>(err: T) -> Self {
        Self::Stringified(format!("{}", err))
    }
}

// -------------
// Ignore: The following dummy types exist only to make the demo compile.  In practice these types would be defined
// elsewhere in the project.
use crate::traits::error_ext::Substrate;
use derive_more::Display;

#[derive(Debug, Display, Clone, PartialEq)]
pub struct Account;
#[derive(Debug, Display, Clone, PartialEq)]
pub struct Balance;
#[derive(Debug, Display, Clone, PartialEq)]
pub struct TransactionId;
#[derive(Debug, Display, Clone, PartialEq)]
pub struct UserId;
#[derive(Debug, Display, Clone, PartialEq)]
pub struct Vote;
