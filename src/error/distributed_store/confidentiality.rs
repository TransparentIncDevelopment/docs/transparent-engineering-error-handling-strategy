#[cfg(feature = "substrate")]
pub mod substrate;

use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Error: Create amount must be greater than zero.  Attempted to create {0} Xand.")]
    CreateAmountMustBeMoreThanZero(Balance),
    #[error(
        "Error: Could not find the specified nonce ({1}) on a Create transaction ({0}) to fulfill"
    )]
    NoMatchingCreateNonceToFulfill(TransactionId, Nonce),
    // ...
}

// -------------
// Ignore: The following dummy types exist only to make the demo compile.  In practice these types would be defined
// elsewhere in the project.
use derive_more::Display;

#[derive(Debug, Display, Clone, PartialEq)]
pub struct Balance;
#[derive(Debug, Display, Clone, PartialEq)]
pub struct Nonce;
#[derive(Debug, Display, Clone, PartialEq)]
pub struct TransactionId;
