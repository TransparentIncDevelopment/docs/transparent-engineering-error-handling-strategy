use super::{
    confidentiality::substrate::ConfidentialityFailure, transaction::substrate::TransactionFailure,
    validator_emissions::substrate::ValidatorEmissionsFailure, Error as DistributedStoreError,
};
use crate::traits::error_ext;

impl<R: Debug> error_ext::Substrate<R> for DistributedStoreError {
    type Error = DispatchError;

    fn as_substrate_error(&self) -> Self::Error {
        match self {
            DistributedStoreError::ConfidentialityError(err) => {
                let substrate_err: ConfidentialityFailure<R> = err.as_substrate_error();
                substrate_err.into()
            }
            DistributedStoreError::TransactionError(err) => {
                let substrate_err: TransactionFailure<R> = err.as_substrate_error();
                substrate_err.into()
            }
            DistributedStoreError::ValidatorEmissionsError(err) => {
                let substrate_err: ValidatorEmissionsFailure<R> = err.as_substrate_error();
                substrate_err.into()
            }
        }
    }
}

// -------------
// Ignore: The following dummy types exist only to make the demo compile.  In practice these types would be defined
// elsewhere in the project.
use crate::traits::error_ext::Substrate;
use std::fmt::Debug;
use thiserror::Error;

#[derive(Debug, Error, Clone, PartialEq)]
#[error("Dummy type")]
pub struct DispatchError;
