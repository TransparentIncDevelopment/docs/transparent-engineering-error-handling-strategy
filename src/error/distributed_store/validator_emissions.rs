#[cfg(feature = "substrate")]
pub mod substrate;

use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Error: User Id {0} not found in authority set.")]
    IdNotInAuthoritySet(UserId),
    // ...
}

// -------------
// Ignore: The following dummy types exist only to make the demo compile.  In practice these types would be defined
// elsewhere in the project.
use derive_more::Display;

#[derive(Debug, Display, Clone, PartialEq)]
#[display(fmt = "{:?}", validators)]
pub struct AuthoritySet {
    validators: Vec<UserId>,
}
#[derive(Debug, Display, Clone, PartialEq)]
pub struct UserId;
#[derive(Debug, Display, Clone, PartialEq)]
pub struct TransactionId;
