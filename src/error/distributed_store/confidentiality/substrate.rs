use super::Error as ConfidentialityError;
use crate::traits::error_ext;

impl<R: Debug> error_ext::Substrate<R> for ConfidentialityError {
    type Error = ConfidentialityFailure<R>;

    fn as_substrate_error(&self) -> ConfidentialityFailure<R> {
        match self {
            ConfidentialityError::CreateAmountMustBeMoreThanZero(balance) => {
                crate::logging::log_information_before_data_loss(balance);
                ConfidentialityFailure::CreateAmountMustBeMoreThanZero
            }

            ConfidentialityError::NoMatchingCreateNonceToFulfill(transaction_id, nonce) => {
                crate::logging::log_information_before_data_loss((transaction_id, nonce));
                ConfidentialityFailure::NoMatchingCreateNonceToFulfill
            }
        }
    }
}

// -------------
// Ignore: The following dummy types exist only to make the demo compile.  In practice these types would be defined
// elsewhere in the project.
use std::{fmt::Debug, marker::PhantomData};
use thiserror::Error;

#[derive(Debug, Error)]
#[error("Dummy type")]
pub enum ConfidentialityFailure<R: Debug> {
    CreateAmountMustBeMoreThanZero,
    NoMatchingCreateNonceToFulfill,
    _PhantomData(PhantomData<R>),
}
impl<R: Debug> From<ConfidentialityFailure<R>>
    for crate::error::distributed_store::substrate::DispatchError
{
    fn from(_: ConfidentialityFailure<R>) -> Self {
        Self
    }
}
