use super::Error as TransactionError;
use crate::traits::error_ext;

impl<R: Debug> error_ext::Substrate<R> for TransactionError {
    type Error = TransactionFailure<R>;

    fn as_substrate_error(&self) -> TransactionFailure<R> {
        match self {
            TransactionError::Unsigned(transaction_id) => {
                crate::logging::log_information_before_data_loss(transaction_id);
                TransactionFailure::Unsigned
            }

            TransactionError::OnlySuperuserMayRegisterMembers(user_id) => {
                crate::logging::log_information_before_data_loss(user_id);
                TransactionFailure::OnlySuperuserMayRegisterMembers
            }

            TransactionError::MintAmountMustBeMoreThanZero(balance) => {
                crate::logging::log_information_before_data_loss(balance);
                TransactionFailure::MintAmountMustBeMoreThanZero
            }

            TransactionError::SmeltAmountMustBeMoreThanZero(balance) => {
                crate::logging::log_information_before_data_loss(balance);
                TransactionFailure::SmeltAmountMustBeMoreThanZero
            }

            TransactionError::VoteAlreadySubmittedForAccount(vote, account) => {
                crate::logging::log_information_before_data_loss((vote, account));
                TransactionFailure::VoteAlreadySubmittedForAccount
            }

            TransactionError::WrappedThirdPartyError(wrapped_err) => {
                crate::logging::log_information_before_data_loss(wrapped_err);
                TransactionFailure::WrappedThirdPartyFailure // Here you could return more specific errors based on the boxed errors type (if casted)
            }
        }
    }
}

// -------------
// Ignore: The following dummy types exist only to make the demo compile.  In practice these types would be defined
// elsewhere in the project.
use std::{fmt::Debug, marker::PhantomData};
use thiserror::Error;

#[derive(Debug, Error)]
#[error("Dummy type")]
pub enum TransactionFailure<R: Debug> {
    Unsigned,
    OnlySuperuserMayRegisterMembers,
    MintAmountMustBeMoreThanZero,
    SmeltAmountMustBeMoreThanZero,
    VoteAlreadySubmittedForAccount,
    WrappedThirdPartyFailure,
    WrappedThirdPartyNonErrorFailure,
    _PhantomData(PhantomData<R>),
}
impl<R: Debug> From<TransactionFailure<R>>
    for crate::error::distributed_store::substrate::DispatchError
{
    fn from(_: TransactionFailure<R>) -> Self {
        Self
    }
}
