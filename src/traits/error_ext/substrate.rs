use std::error::Error;
use std::fmt::Debug;

pub trait Substrate<Runtime: Debug> {
    type Error: Error;

    fn as_substrate_error(&self) -> Self::Error;
}
